# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.1.1] - 26/08/2023
- drupal_get_path() and drupal_get_filename() have been deprecated in favor of extension listing services https://www.drupal.org/node/2940438 replaced all occurrences

## [3.1.0] - 26/05/2023
- Added destination query parameter to be able to redirect users to the page they were requesting before be redirected to log-in. This minor change is related with the new version, 3.0.0 of the openid module.

## [3.0.1] - 07/11/2022
- Resolve issues in `screen.css` causing the toolbar to miss padding on the sides.

## [3.0.0] - 14/09/2022

- Prepared for php 8.1, fixed warnings

## [2.2.4] - 03/12/2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x` from composer.

## [2.2.3] - 08/02/2021

- Add core: 8.x to fix enabling issue

## [2.2.2] - 13/01/2021

- Update module to be D9-ready

## [2.2.1] - 04/12/2020

- Add composer.json file

## [2.2.0] - 19/07/2019

- Fixed toolbar not working in non-CERN themes

## [2.1.2] - 10/05/2019

- Fixed issue with deprecated link l() function

## [2.1.1] - 22/02/2019

- Fixed issue of toolbar admin menu crashing

## [2.1.0] - 07/12/2018

- Changed file structure
